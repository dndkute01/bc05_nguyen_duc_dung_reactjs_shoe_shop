import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { name, price, description, image } = this.props.detail;
    return (
      <div className="row">
        <img src={image} className="col-4" alt="" />
        <div className="col-8">
          <p>{name}</p>
          <p>{price}</p>
          <p>{description}</p>
        </div>
      </div>
    );
  }
}
