import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe,
    cart: [],
  };
  handleDetail = (value) => {
    this.setState({
      detail: value,
    });
  };
  handleAddToCart = (value) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == value.id;
    });

    if (index == -1) {
      let cartItem = { ...value, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }

    this.setState({
      cart: cloneCart,
    });
  };

  handlePlus = (value) => {};
  render() {
    return (
      <div className="container">
        <Cart
          data={this.state.shoeArr}
          handlePlus={this.handlePlus}
          cart={this.state.cart}
        />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleDetail={this.handleDetail}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
