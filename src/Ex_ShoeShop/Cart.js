import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.number}</td>
          <td>
            <button className="btn btn-outline-secondary ">-</button>
            <span className="mx-1">{item.number}</span>
            <button
              onClick={() => {
                this.props.handlePlus(this.props.data);
              }}
              className="btn btn-outline-secondary"
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: "100px" }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
