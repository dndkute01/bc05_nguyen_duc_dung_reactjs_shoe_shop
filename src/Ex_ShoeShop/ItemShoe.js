import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.data;
    return (
      <div className="col-3 m-3 mx-auto">
        <div className="card text-center h-100 ">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
          </div>

          <button
            onClick={() => {
              this.props.handleAddToCart(this.props.data);
            }}
            className="btn btn-outline-success "
          >
            BUY
          </button>
          <button
            onClick={() => {
              this.props.handleDetail(this.props.data);
            }}
            className="btn btn-outline-primary "
          >
            DETAIL
          </button>
        </div>
      </div>
    );
  }
}
